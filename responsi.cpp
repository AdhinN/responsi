#include <iostream>
#define MAX 50
using namespace std;

void push(char);
char pop();
void convert(string);
bool isFull();
bool isEmpty();
char stk[MAX];
int top = -1;
void cetak();

int main(){
    string kode;
    cout << "Masukkan kode pesan: ";
    cin >> kode;
    convert(kode);
    cout << "Arti pesan: ";
    cetak();

    return 0;
}

bool isFull(){
    if(top == MAX-1){
        return true;
    } else {
        return false;
    }
}

bool isEmpty(){
    if(top == -1){
        return true;
    } else {
        return false;
    }
}

void push(char oper){
    if(isFull()){
        cout << "Stack Penuh" << "\n";
    } else {
        top++;
        stk[top] = oper;
    }
}

char pop(){
    char ch;
    if(isEmpty()){
        cout << "Stack Kosong" << "\n";
    } else {
        ch = stk[top];
        stk[top] = '\0';
        top--;
        return ch;
    }
    return 0;
}

void convert(string kode){
    int i = 0;
    while(kode[i] != '\0'){
        if((kode[i] >= 'A' && kode[i] <= 'Z') || (kode[i] >= 'a' && kode[i] <= 'z')){
            push(kode[i]);
            i++;

        } else if (kode[i] == '*'){
            push(kode[i]);
            pop();
            i++;
        }
    }
}

void cetak(){
    for(int i = 0; i <= top; i++){
        cout << stk[i];
    }
}
